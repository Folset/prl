﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {
	static MusicPlayer instance = null;


	void Awake () {
		if (instance != null) {
			Destroy (gameObject);
			print ("Duplicate music player killed. ");
		} else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
		}
	}

	void Start () {
		
	}

	void Update () {
		
	}
}
