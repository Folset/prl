﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitlePulse : MonoBehaviour {

		public Text title;
		bool pulseRev = false;
		public Color pulseC;
		float pulse = 1f;
		float pulseMax = 1f;
		float pulseMin = 0.25f;

		// Update is called once per frame
		void Update () {
			if (pulse >= pulseMax) {
				pulseRev = true;
			} else if (pulse < pulseMin) {
				pulseRev = false;
			}
			if (pulseRev == false) {
				pulse += 0.0125f;
			} else {
				pulse -= 0.0125f;
			}
			pulseC = new Color (pulse, 0f, 0f);
			this.title.color = pulseC;
		}
}