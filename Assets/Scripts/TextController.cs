﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

	public Text text;
	public Text header;
	// Begin state machine, which are actually the rooms you can enter.
	public enum States
	{
		engine, bunks, mess, cockpit, cargo, hallway, pod, gstart, end, inv, credit
	}
	public States myState;
	// These are the items you will need. Don't need all of them, but would be good to have for reasons.
	private bool crowbar = false;
	private bool picture = false;
	private bool herbs = false;
	private bool components = false;
	private bool necklace = false;
	private bool food = false;
	private bool bag = false;
	private bool poddoor = false;
	private int textblock = 1; //Chooses which textblock to use
	private bool inventory = false; //Inventory Toggle
	private States currentState;

	// State machine stuff
	void Start () {
		myState = States.gstart;
		currentState = States.gstart;
	}

	// Update is called once per frame
	void Update () {
		//print (myState);
		//print (textblock);
		if 		(myState == States.gstart) 		{gstart (); header.text = "???"; }
		else if (myState == States.cargo) 		{cargo (); header.text = "Cargohold";}
		else if (myState == States.hallway) 	{hallway (); header.text = "Hallway";}
		else if (myState == States.engine) 		{engine (); header.text = "Engine Room";}
		else if (myState == States.bunks) 		{bunks (); header.text = "Crew Bunks";}
		else if (myState == States.mess) 		{mess (); header.text = "Messhall";}
		else if (myState == States.cockpit) 	{cockpit (); header.text = "Cockpit";}
		else if (myState == States.pod) 		{pod (); header.text = "Escape Pod";}
		else if (myState == States.end) 		{end (); header.text = "Game Over";}
		else if (myState == States.inv) 		{inv (); header.text = "Inventory";}
		else if (myState == States.credit) 		{credit (); header.text = "Credits";}
		//Inventory and Quit Game
		//if (Input.GetKeyDown(KeyCode.Escape)) {Application.Quit();}
		if (Input.GetKeyDown(KeyCode.Tab) & inventory == false) {currentState = myState; myState = States.inv; inventory = true;}
		else if (Input.GetKeyDown(KeyCode.Tab) & inventory == true) {myState = currentState; currentState = myState; inventory = false;}
	}

	void inv () {
		if (crowbar == false || picture == false || herbs == false || components == false || necklace == false || food == false || bag == false) {text.text = "You have a look through what you have so far, and feel like you're forgetting something. You look up at the pulsing red light and wonder what it might be.\n\n\n[TAB] Close inventory";} 
		else if (crowbar == true & picture == true & herbs == true & components == true & necklace == true & food == true & bag == true) {text.text = "Looking through everything you've collected, you feel confident that you've gotten all you need to survive until help arrives. Better head to the Escape Pod.\n\n\n[TAB] Close inventory";}
	}
	#region Textblocks
	void gstart() {
		switch (textblock) 
		{
		case 1:
			text.text = "A splitting headache pulls you from the sea of unconsciousness. Debris covers you and the room pulsing with red light. Blood drips from your brow. Looking around the room you see a wall of sealed containers and a closed door labeled Hallway. Something seems to be lying under the debris next to you." +
				"\n\n\n[C] Clear the debris";
			break;
		case 2:
			text.text = "You clear the debris to find a portable computer console. You pick it up, it fits snugly in your hand. Wiping the screen of dust, it turns on - blinding you for a moment, your headache intensifies in retaliation. It displays a logbook of containers and their contents, seems like they are all filled with medicinal herbs and valuable components. " +
				"\n\n\n[H] Go to hallway\n[U] Unlock a container and take some herbs";
			break;
		case 3:
			text.text = "You tap unlock next to one of the containers listed on the console and grab a bag of medicinal herbs out of the container, shoving the bag into your pocket. This should at least help with the headache once you get to safety." +
				"\n\n\n[H] Go to hallway";
			break;
		default :
			text.text = "Error";
			print ("Error at " + myState + " Textblock:"+ textblock);
			break;
		}

		if (Input.GetKeyDown (KeyCode.C)) {textblock = 2;}
		else if (Input.GetKeyDown (KeyCode.U)) {herbs = true; textblock = 3;}
		else if (Input.GetKeyDown (KeyCode.H)) {textblock = 1; myState = States.hallway;}
	}

	void cargo() {
		switch (textblock) 
		{
		case 1:
			text.text = "You re-enter the cargo hold, the pulsing red light and sight of debris makes your head sting slightly. Sealed containers fill the room." +
			"\n\n\n[U] Unlock a container and put some herbs in your pocket for later use";
			break;
		case 2:
			text.text = "You tap unlock next to one of the containers listed on the console and grab a bag of medicinal herbs out of the container, shoving the bag into your pocket. This should at least help with the headache once you get to safety." +
				"\n\n\n[H] Go to hallway";
			break;
		case 3:
			text.text = "Minding the debris as you cross the cargo hold pulsing with red light, you walk over to a container filled with valuable components and unlock it with your console. No use letting the whole haul go to waste." +
				"\n\n\n[H] Stash some valuable components in your bag and return to the hallway.";
			break;
		case 4:
			text.text = "You think about returning to the cargo hold to try save as much of the haul as you can, but you're going to need something to put the components in. Maybe there's a bag somewhere?" +
				"\n\n\n[H] Return to hallway";
			break;
		case 5:
			text.text = "You feel like you've stashed enough and should probably do something else." +
				"\n\n\n[H] Return to hallway";
			break;
		}

		if (Input.GetKeyDown (KeyCode.U)) {herbs = true; textblock = 2;}
		else if (Input.GetKeyDown (KeyCode.H) & bag == true) {components = true; herbs = true; textblock = 1; myState = States.hallway;}
		else if (Input.GetKeyDown (KeyCode.H)) {textblock = 1; myState = States.hallway;}
	}
	void hallway() {
		if (textblock == 1) {
			text.text = "You enter a hallway pulsing red, filled with nothing but labeled doors.\n\n\n\n" +
			"[E] Enter the Engine room\n[B] Go to crew Bunks\n[M] Enter the Messhall\n" +
			"[C] Enter the Cockpit\n[H] Enter the Cargo Hold\n[P] Enter the Escape Pod";
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			textblock = 1;
			myState = States.engine;
		} else if (Input.GetKeyDown (KeyCode.B)) {
			textblock = 1;
			myState = States.bunks;
		} else if (Input.GetKeyDown (KeyCode.M)) {
			if (food == true) {
				textblock = 4;
				myState = States.mess;
			} else {
				textblock = 1;
				myState = States.mess;
			}
		}
		else if (Input.GetKeyDown (KeyCode.C)) {
			if (necklace == true) {
				textblock = 4;
				myState = States.cockpit;
			} else {
				textblock = 1;
				myState = States.cockpit;
			}
		}
		else if (Input.GetKeyDown (KeyCode.P)) {if (poddoor == true) {
				textblock = 3;
				myState = States.pod;
		} else if (poddoor == false & crowbar == true) {
				textblock = 1;
				myState = States.pod;
			}
		else if (poddoor == false & crowbar == false) {
			textblock = 4;
			myState = States.pod;
		}
	}
		else if (Input.GetKeyDown (KeyCode.H)) {
			if (herbs == true & components == true) {textblock = 2; text.text = "You feel like you've stashed enough herbs, and should probably do something else.\n\n\n" +
					"[E] Enter the Engine room\n[B] Go to crew Bunks\n[M] Enter the Mess hall\n" +
					"[C] Enter the Cockpit\n[P] Enter the Escape Pod";}
			else if (herbs == true & components == false & bag == false) {textblock = 4; myState = States.cargo;}
			else if (herbs == true & components == false & bag == true) {textblock = 3; myState = States.cargo;}
			else {textblock = 1; myState = States.cargo;}
			}
	}

	void engine() {
		switch (textblock) 
		{
		case 1:
			text.text = "You enter the engine room. It's quiet. Besides the sleeping engine and a pulsing red light, there's a workstation and a tool bench." +
			"\n\n\n[W] Check the workstation\n[T] Check the tool bench\n[H] Return to hallway";
			break;
		case 2:
			text.text = "As you stand next to the Workstation, your console vibrates and the Workstation's monitor comes alive, blinding you again - your head rings with pain." +
				"\nA message on the monitor reads:\n\nENGINE OFFLINE - SYSTEM CONTROL MALFUNCTION\nCOMMS OFFLINE - SYSTEM CONTROL MALFUNCTION\nLIFE SUPPORT OFFLINE - SYSTEM CONTROL MALFUNCTION" +
				"\n\n\n[T] Check the tool bench\n[H] Return to hallway";
			break;
		case 3:
			text.text = "You walk up to the toolbench. Looking for something useful amongst the clutter, you find a crowbar and pick it up." +
				"\n\n\n[W] Check the workstation\n[H] Return to hallway";
			break;
		case 4:
			text.text = "Your head must really be hurting, you already have the crowbar." +
				"\n\n\n[W] Check the workstation\n[H] Return to hallway";
			break;
		}

		if (Input.GetKeyDown (KeyCode.W)) {textblock = 2;}
		else if (Input.GetKeyDown (KeyCode.T) & crowbar == true) {textblock = 4;}
		else if (Input.GetKeyDown (KeyCode.T)) {textblock = 3; crowbar = true;}
		else if (Input.GetKeyDown (KeyCode.H)) {textblock = 1; myState = States.hallway;}

	}

	void bunks() {
		switch (textblock) 
		{
		case 1:
			text.text = "Upon entering the bunk room, you see a single queen sized bunk - a footlocker at it's end. The room pulsates with red light." +
				"\n\n\n[C] Check the foot locker\n[H] Return to hallway";
			break;
		case 2:
			text.text = "You walk up to the footlocker and your console vibrates. You check it and it tells you that your footlocker has been unlocked. You open the locker and see a large leather messenger bag and a faded picture in a silver frame inscribed 'Together Always'. Your stomach drops, your head sears." +
				"\n\n\n[T] Take the bag and picture frame\n[H] Return to hallway";
			break;
		case 3:
			text.text = "You shakingly grab the bag and sling it over yourself, putting the faded picture inside. There is a lot of room in there, best fill it with supplies." +
				"\n\n\n[H] Return to hallway";
			break;
		case 4:
			text.text = "You've already taken everything you need here." +
				"\n\n\n[H] Return to hallway";
			break;
		}

		if (Input.GetKeyDown (KeyCode.C) & picture == true) {textblock = 4;}
		else if (Input.GetKeyDown (KeyCode.C)) {textblock = 2;}
		else if (Input.GetKeyDown (KeyCode.H)) {textblock = 1; myState = States.hallway;}
		else if (Input.GetKeyDown (KeyCode.T) & textblock == 2) {textblock = 3; bag = true; picture = true;}
	}
	void mess() {
		switch (textblock) 
		{
		case 1:
			text.text = "You enter the mess greeted by a pulsing red light. A table is flanked by two chairs, built into the wall is a food and drink dispenser." +
				"\n\n\n[C] Check the dispenser\n[H] Return to hallway";
			break;
		case 2:
			text.text = "The dispenser seems to be split into two parts, one side for drinks and the other for food.\nYou're going to need something to hold the supplies." +
				"\n\n\n[H] Return to hallway";
			break;
		case 3:
			text.text = "You swing your bag around and open it as you get a few rations of food and drink from the dispenser." +
				"\n\n\n[H] Return to hallway";
			break;
		case 4:
			text.text = "You open your bag and mentally measure the remaining space, coming to the conclusion that you probably won't be able to fit much else in there." +
				"\n\n\n[H] Return to hallway";
			break;
		}

		if (Input.GetKeyDown (KeyCode.C) & bag == true) {textblock = 3; food = true;}
		else if (Input.GetKeyDown (KeyCode.C)) {textblock = 2;}
		else if (Input.GetKeyDown (KeyCode.H)) {textblock = 1; myState = States.hallway;}
	}

	void cockpit() {
		switch (textblock) 
		{
		case 1:
			text.text = "You enter the room and see a large hole in the window on the pilot's side. The blast shields are covering the cockpit window, preventing the air from being sucked out the holes spread across it.\n\nYou can see someone sitting in the pilot’s seat, but they aren’t moving. There’s a large bulge at the back of the seat. The ship's controls are completely destoryed. A red light pulsates." +
				"\n\n\n[C] Check the pilot\n[H] Return to hallway";
			break;
		case 2:
			text.text = "You step deeper into the cockpit, blood splatting as you step. You get close enough to see the pilot's face and your head breaks out into excruciating pain, crippling you for a moment as you remember everything - while losing it at all in the same moment." +
				"\n\n\n[T] Take lover's necklace.";
			break;
		case 3:
			text.text = "You take your passed lover's necklace, as you give them one last kiss." +
				"\n\n\n[H] You stagger back into the hallway, tears stream down your face.";
			break;
		case 4:
			text.text = "Your heart aches and your legs refuse to go any further." +
				"\n\n\n[H] Return to hallway";
			break;
		}

		if (Input.GetKeyDown (KeyCode.C)) {textblock = 2;}
		else if (Input.GetKeyDown (KeyCode.T)) {textblock = 3; necklace = true;}
		else if (Input.GetKeyDown (KeyCode.H)) {textblock = 1; myState = States.hallway;}
	}

	void pod() {
		switch (textblock) 
		{
		case 1:
			text.text = "You try opening the escape pod door, but the ship's electronics seem to be malfunctioning. The door is jammed shut, you need to find a way to open it somehow." +
				"\n\n\n[C] Use the crowbar to pry open the door\n[H] Return to hallway";
			break;
		case 2:
			text.text = "With some effort, you manage to pry open the malfunctioning door. Amazed at how well crowbars have stood the test of time, you put it away and climb into the escape pod and take one of the two available seats, staring back at the pulsing red light." +
				"\n\n\n[E] Attempt to close the escape pod doors and eject\n[H] Return to hallway";
			break;
		case 3:
			text.text = "You enter the escape pod and take one of the two available seats, staring back at the pulsing red light." +
				"\n\n\n[E] Attempt to close the escape pod doors and eject\n[H] Return to hallway";
			break;
		case 4:
			text.text = "You try opening the escape pod door, but the ship's electronics seem to be malfunctioning. The door is jammed shut, you need to find a way to open it somehow." +
				"\n\n\n[H] Return to hallway";
			break;
		}

		if (Input.GetKeyDown (KeyCode.C) & crowbar == true) {
			textblock = 2;
			poddoor = true;
		} else if (Input.GetKeyDown (KeyCode.H)) {
			textblock = 1;
			myState = States.hallway;
		} else if (Input.GetKeyDown (KeyCode.E)) {
			if (poddoor == true & picture == true & herbs == true & components == true & necklace == true & food == true) {
				textblock = 1;
				myState = States.end;
			} else if (poddoor == true & picture == false || herbs == false || components == false || necklace == false || food == false) {
				textblock = 2;
				myState = States.end;
			}
		}
	}
	void end() {
		if (textblock == 1) {
			text.text = "You strap yourself in the seat and hit eject. The escape pod doors close with a struggle, choking the pulsing red light in the ship’s hallway before closing. Your body jerks as the escape pod detaches itself right before the engines propel the pod away from your ship, your love, and the red pulsing light.\n\nYou managed to collect enough supplies to keep you physically and mentally healthy for the month you spent drifting through space. The faded picture and necklace kept you strong while you waited for help to arrive.\n\nLater, you found out that a meteor shower made it's way through your ship, which caused a series of electrical malfunctions as well as the death of the pilot... Your lover.\n\nThanks to the valuable components you saved, you managed to buy a new ship and named it after your past lover.\n\n" +
				"[C] Credits\n[R] Restart";	
		} else if (textblock == 2) {
			if (food == false) {
				text.text = "You strap yourself in the seat and hit eject. The escape pod doors close with a struggle, choking the pulsing red light in the ship’s hallway before closing. Your body jerks as the escape pod detaches itself right before the engines propel the pod away from your ship, your love, and the red pulsing light.\n\nIt took a month before anyone managed to get to your escape pod to find your corpse. An investigation concluded that if only you had more food, you might have survived long enough for them to save you.\n\n\n" +
					"[C] Credits\n[R] Restart";
			} else if (herbs == false) {
				text.text = "You strap yourself in the seat and hit eject. The escape pod doors close with a struggle, choking the pulsing red light in the ship’s hallway before closing. Your body jerks as the escape pod detaches itself right before the engines propel the pod away from your ship, your love, and the red pulsing light.\n\nIt took a month before anyone managed to get to your escape pod, only to find it empty, the door left open after you ejected yourself into the cold embrace of space.\n\nAn investigation team found your console and a note in it saying you couldn't bare to live without your lover, how life didn't seem worth it anymore. If only you had something to help you cope with the emotional pain of your loss.\n\n" +
					"[C] Credits\n[R] Restart";	
			} else if (necklace == false) { 
				text.text = "You strap yourself in the seat and hit eject. The escape pod doors close with a struggle, choking the pulsing red light in the ship’s hallway before closing. Your body jerks as the escape pod detaches itself right before the engines propel the pod away from your ship, and the red pulsing light.\n\nIt took a month before anyone managed to get to your escape pod and save you.\n\nWithout anything more solid than a faded picture to remember your past love, you grew cold to the world. You turned to a short life of crime before being gunned down.\n\n" +
					"[C] Credits\n[R] Restart";	
			}
			else if (components == false) { 
				text.text = "You strap yourself in the seat and hit eject. The escape pod doors close with a struggle, choking the pulsing red light in the ship’s hallway before closing. Your body jerks as the escape pod detaches itself right before the engines propel the pod away from your ship, your love, and the red pulsing light.\n\nYou managed to collect enough supplies to keep you physically and mentally healthy for the month you spent drifting through space waiting for help.\n\nYou later found out that a meteor shower made it's way through your ship, which caused a series of electrical malfunctions as well as the death of the pilot... Your lover.\n\nYou were never able to afford another ship, you regret not saving anything from the cargo hold in your bag. At least you have your memories and your life.\n\n" +
					"[C] Credits\n[R] Restart";				
			}
		}
		#endregion
		if (Input.GetKeyDown (KeyCode.C)) {textblock = 1;  myState = States.credit;}
		else if (Input.GetKeyDown (KeyCode.R)) {
			crowbar = false;
			picture = false;
			herbs = false;
			components = false;
			necklace = false;
			food = false;
			bag = false;
			poddoor = false;
			textblock = 1;
			inventory = false;
			currentState = States.gstart;
			myState = States.gstart;
		}
	}

	void credit() {
		text.text = "THANK YOU FOR PLAYING\nPulsing Red Light - A Textbased adventure micro-game created for fun & practice by:\n\nDESIGN, CODE & STORY by\n       Tyrone J. Swart   ( @aka_Folset )\n\n" +
			"MUSIC & STORY EDITING by\n       Damian Ross   ( @thatisvile )\n\n\n" +
			"\n\nCopyright 2017\n\n[R] Restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			crowbar = false;
			picture = false;
			herbs = false;
			components = false;
			necklace = false;
			food = false;
			bag = false;
			poddoor = false;
			textblock = 1;
			inventory = false;
			currentState = States.gstart;
			myState = States.gstart;
		}
	}
}